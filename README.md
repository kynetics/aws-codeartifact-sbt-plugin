# aws-codeartifact-sbt-plugin

---

An SBT plug-in used to automatically refresh the authorization token to publish artifacts to AWS CodeArtifact.

It uses the standard AWS command-line interface authorization mechanism to obtain a fresh authorization token.

## Configuration

Add the following line in your `plugins.sbt` file:

```sbt
addSbtPlugin("com.kynetics" % "aws-codeartifact-sbt-plugin" % "<version>")
```

The plug-in makes use of the following settings to publish artifacts to AWS CodeArtifact:

```sbt
versionScheme := Some("semver-spec")
tokenDuration := Duration.ofMinutes(15)
awsRegion := "us-west-2"
awsOwner := "<aws_account_id>"
awsArtifactDomain := "<codeartifact_domain_name>"
awsArtifactRepoName := "<codeartifact_repo_name>"

// [Optional] Set to true if you want to publish an SBT plug-in (default: false)
isSbtPlugin := false
```

### Additional dependencies for SBT plug-ins

If you are using `aws-codeartifact-sbt-plugin` to publish another SBT plug-in, also add the following line to your
`plugins.sbt` file:

```sbt
addSbtPlugin("com.scalawilliam.esbeetee" % "sbt-vspp" % "<sbt-vspp-version>")
```

Please refer to [sbt-vspp documentation](https://github.com/esbeetee/sbt-vspp) to understand why this is necessary.

To publish an SBT plug-in, run `sbt publishPlugin`, instead of `sbt publish`. This will not only publish the artifacts
on CodeArtifact, but also update the status of the newly-published package to `Published`. Please refer to the
[CodeArtifact documentation](https://docs.aws.amazon.com/codeartifact/latest/ug/maven-curl.html) for additional details.
