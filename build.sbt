import Dependencies._

ThisBuild / organization := "com.kynetics"
ThisBuild / scalaVersion := "2.12.16"

lazy val `aws-codeartifact-sbt-plugin` =
  project
    .in(file("."))
    .settings(name := "aws-codeartifact-sbt-plugin")
    .settings(commonSettings)
    .settings(dependencies)
    .enablePlugins(SbtPlugin)
    .settings(
      pluginCrossBuild / sbtVersion := {
        scalaBinaryVersion.value match {
          case "2.12" => "1.7.1" // set minimum sbt version
        }
      },
      scriptedLaunchOpts := { scriptedLaunchOpts.value ++
        Seq("-Xmx1024M", "-Dplugin.version=" + version.value)
      },
      scriptedBufferLog := false,
      publishMavenStyle := true
    )

lazy val commonSettings =
  compilerPlugins ++ commonScalacOptions ++ Seq(
    update / evictionWarningOptions := EvictionWarningOptions.empty
  )

lazy val compilerPlugins = Seq(
  addCompilerPlugin(com.olegpy.`better-monadic-for`),
  addCompilerPlugin(org.augustjune.`context-applied`),
  addCompilerPlugin(org.typelevel.`kind-projector`),
)

lazy val commonScalacOptions = Seq(
  Compile / console / scalacOptions := {
    (Compile / console / scalacOptions)
      .value
      .filterNot(_.contains("wartremover"))
      .filterNot(Scalac.Lint.toSet)
      .filterNot(Scalac.FatalWarnings.toSet) :+ "-Wconf:any:silent"
  },
  Test / console / scalacOptions :=
    (Compile / console / scalacOptions).value,
)

lazy val dependencies = Seq(
  libraryDependencies ++= Seq(
    software.amazon.awssdk.codeartifact,
    software.amazon.awssdk.sts
  )
)
