versionScheme := Some("semver-spec")

ThisBuild / organizationName := "Kyentics"
ThisBuild / organizationHomepage := Some(url("https://www.kynetics.com/"))
ThisBuild / description := "An SBT plug-in to simplify the publishing of artifacts to CodeArtifact. It automatically" +
  "fetches an authorization token and uses it to publish the artifacts."

ThisBuild / scmInfo := Some(
  ScmInfo(
    url("https://bitbucket.org/kynetics/aws-codeartifact-sbt-plugin/"),
    "scm:git@bitbucket.org:kynetics/aws-codeartifact-sbt-plugin.git"
  )
)

ThisBuild / developers := List(
  Developer(
    id    = "andrea.zoleo",
    name  = "Andrea Zoleo",
    email = "andrea.zoleo@kynetics.com",
    url   = url("https://www.kynetics.com/")
  ),
  Developer(
    id = "matteo.dipirro",
    name = "Matteo Di Pirro",
    email = "matteo.dipirro@kynetics.com",
    url = url("https://www.kynetics.com/")
  )
)

ThisBuild / licenses := List("Apache 2" -> new URL("http://www.apache.org/licenses/LICENSE-2.0.txt"))

ThisBuild / homepage := Some(url("https://bitbucket.org/kynetics/aws-codeartifact-sbt-plugin/"))

// Remove all additional repository other than Maven Central from POM
ThisBuild / pomIncludeRepository := { _ => false }

ThisBuild / sonatypeCredentialHost := "s01.oss.sonatype.org"
ThisBuild / publishTo := sonatypePublishToBundle.value
ThisBuild / publishMavenStyle := true

usePgpKeyHex("3518B55E76D38F3ACD9459B11D071FE6A2132D37")
